# apache superset testing

> Author *`michaelspring123@web.de`*
> > Creation date *`2020-12-26`*
> > > version *`alpha`*


# configuration with docker desktop

## short overview


```mermaid
graph LR
    id1(configure containers)-->id2(get internal ip)-->id3(connect superset to pg)
    style id1 fill:#f9f,stroke:#333,stroke-width:4px
    style id2 fill:#bbf,stroke:#f66,stroke-width:2px,color:#fff,stroke-dasharray: 5 5
    style id3 fill:#f9,stroke:#333,stroke-width:2px
```

### get started

the first and easiest configuration is done with docker *`desktop on macOS`*. A standard postgres container and a standard superset container are used here.

## postgres:alpine

```{r, engine='bash', count_lines}
docker run -d --name posttest postgres:alpine -e POSTGRES_PASSWORD=fred -p 5432:5432
```

## apache/superset

> *`docker pull apache/superset`*

the steps are exactly the same as the official superset docker documentation

go to [docker.com/r/apache/superset](https://hub.docker.com/r/apache/superset) to use the documentation.


# the running container

if everything went well it should look like this. With *`docker ps`* you can see the currently running containers.


```{r, engine='bash', count_lines}
Last login: Sat Oct 23 11:19:17 on ttys000
asdf@asdfs-MacBook-Pro ~ % docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                   PORTS                    NAMES
cc8b197c589a        postgres:alpine     "docker-entrypoint.s…"   5 days ago          Up 5 minutes             0.0.0.0:5432->5432/tcp   posttest
49a7ef30409b        apache/superset     "/usr/bin/docker-ent…"   6 days ago          Up 3 minutes (healthy)   0.0.0.0:8080->8088/tcp   superset
asdf@asdfs-MacBook-Pro ~ % docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' cc8b197c589a
172.17.0.2
asdf@asdfs-MacBook-Pro ~ % 
```



## internal ip address of the container

the address is required to connect the superset to the postgres container. the *`localhost`+ is not sufficient here because it cannot be referenced.

> docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' *`container id`*


```{r, engine='bash', count_lines}
docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' cc8b197c589a
```

:zap: useful entries from stackoverflow. :v:

[stackoverflow](https://stackoverflow.com/questions/35928670/docker-container-for-postgres-9-1-not-exposing-port-5432-to-host)


*`172.17.0.2`* is then required as an **ip address** for the superset postgres connection. the **port** is *`5432`* by default.





# test a superset dashboard with the postgres data


![Screenshot_23_10_21__12_43.jpg](screenshots/Screenshot_23_10_21__12_43.jpg)






# ubuntu-20.04-live-server-amd64.iso

sudo apt update
sudo apt -y upgrade


root@asdf:~/apache-superset-testing/superset-image# python3 -V
Python 3.8.5

sudo apt install -y python3-pip



![ubuntu-20_04-live-server-amd64_iso__Running_.jpg](screenshots/ubuntu-20_04-live-server-amd64_iso__Running_.jpg)




> root@asdf:~# *`mkdir apache-superset-testing`*



> asdf@asdf:~$ *`chmod -R 777 apache-superset-testing`*


> root@asdf:~# *`cd apache-superset-testing`*


> root@asdf:~/apache-superset-testing# *`mkdir superset-image`*


> root@asdf:~/apache-superset-testing# *`cd superset-image`*


> root@asdf:~/apache-superset-testing/superset-image# *`vim Dockerfile`*


FROM apache/superset
# Switching to root to install the required packages
USER root
# Example: installing the MySQL driver to connect to the metadata database
# if you prefer Postgres, you may want to use `psycopg2-binary` instead
RUN pip3 install psycopg2-binary
# Example: installing a driver to connect to Redshift
# Find which driver you need based on the analytics database
# you want to connect to here:
# https://superset.apache.org/installation.html#database-dependencies
RUN pip install sqlalchemy-redshift
# Switching back to using the `superset` user
USER superset



```dockerfile
FROM apache/superset
# Switching to root to install the required packages
USER root
# Example: installing the MySQL driver to connect to the metadata database
# if you prefer Postgres, you may want to use `psycopg2-binary` instead
RUN pip3 install psycopg2-binary
# Example: installing a driver to connect to Redshift
# Find which driver you need based on the analytics database
# you want to connect to here:
# https://superset.apache.org/installation.html#database-dependencies
RUN pip install sqlalchemy-redshift
# Switching back to using the `superset` user
USER superset
```


docker build -t superset_image .


> root@asdf:~/apache-superset-testing/superset-image# *`docker build -t superset_image .`*





# failed to listen to abstract unix socket

root@asdf:~/apache-superset-testing/superset-image# docker build -t superset_image .
Sending build context to Docker daemon   2.56kB
Step 1/5 : FROM apache/superset
 ---> 12547feaacbc
Step 2/5 : USER root
 ---> Running in e5f38d098240
Removing intermediate container e5f38d098240
 ---> 360f5e675084
Step 3/5 : RUN pip3 install psycopg2-binary
 ---> Running in bb089300405c
failed to listen to abstract unix socket "/containerd-shim/fbfdb19553ed293d895d206520025d65a99df9da7d6d8601b9b6ac874121f992.sock": listen unix /containerd-shim/fbfdb19553ed293d895d206520025d65a99df9da7d6d8601b9b6ac874121f992.sock: bind: permission denied: unknown
root@asdf:~/apache-superset-testing/superset-image# 


sudo apt-get update



apt install udo

# asdf

root@asdf:~# chmod -R 777 apache-superset-testing


root@asdf:~/apache-superset-testing/superset-image# pip install psycopg2-binary
Collecting psycopg2-binary
  Downloading psycopg2_binary-2.8.6-cp38-cp38-manylinux1_x86_64.whl (3.0 MB)
     |████████████████████████████████| 3.0 MB 2.7 MB/s 
Installing collected packages: psycopg2-binary
Successfully installed psycopg2-binary-2.8.6
root@asdf:~/apache-superset-testing/superset-image#




# finally building the image

asdf@asdf:~/apache-superset-testing/superset-image$ sudo docker build -t superset_image .
Sending build context to Docker daemon   2.56kB
Step 1/5 : FROM apache/superset
 ---> 12547feaacbc
Step 2/5 : USER root
 ---> Running in fbfddae71df3
Removing intermediate container fbfddae71df3
 ---> d5158b90f07a
Step 3/5 : RUN pip3 install psycopg2-binary
 ---> Running in fd9030281511
Requirement already satisfied: psycopg2-binary in /usr/local/lib/python3.7/site-packages (2.8.5)
WARNING: You are using pip version 21.0.1; however, version 21.1.1 is available.
You should consider upgrading via the '/usr/local/bin/python -m pip install --upgrade pip' command.
Removing intermediate container fd9030281511
 ---> 9040d4ffd84d
Step 4/5 : RUN pip install sqlalchemy-redshift
 ---> Running in f3a5ed86f71a
Collecting sqlalchemy-redshift
  Downloading sqlalchemy_redshift-0.8.2-py2.py3-none-any.whl (34 kB)
Requirement already satisfied: packaging in /usr/local/lib/python3.7/site-packages (from sqlalchemy-redshift) (20.4)
Requirement already satisfied: SQLAlchemy<2.0.0,>=0.9.2 in /usr/local/lib/python3.7/site-packages (from sqlalchemy-redshift) (1.3.20)
Requirement already satisfied: six in /usr/local/lib/python3.7/site-packages (from packaging->sqlalchemy-redshift) (1.15.0)
Requirement already satisfied: pyparsing>=2.0.2 in /usr/local/lib/python3.7/site-packages (from packaging->sqlalchemy-redshift) (2.4.7)
Installing collected packages: sqlalchemy-redshift
Successfully installed sqlalchemy-redshift-0.8.2
WARNING: You are using pip version 21.0.1; however, version 21.1.1 is available.
You should consider upgrading via the '/usr/local/bin/python -m pip install --upgrade pip' command.
Removing intermediate container f3a5ed86f71a
 ---> 91d80c3c4413
Step 5/5 : USER superset
 ---> Running in ce485d3c338f
Removing intermediate container ce485d3c338f
 ---> 1e01824de8ac
Successfully built 1e01824de8ac
Successfully tagged superset_image:latest
asdf@asdf:~/apache-superset-testing/superset-image$ 


# docker images

> asdf@asdf:~$ *`sudo docker images`*

REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
superset_image      latest              1e01824de8ac        6 minutes ago       1.43GB
apache/superset     latest              12547feaacbc        14 hours ago        1.43GB
asdf@asdf:~$



docker run --rm -P -p 127.0.0.1:5432:5432 -e POSTGRES_PASSWORD="1234" --name pg postgres:alpine

# postgres

*`docker run --rm -P -p 0.0.0.0:5432:5432 -e POSTGRES_PASSWORD=asdf --name pg postgres:alpine`*

# test

docker run --name some-postgres -p 0.0.0.0:5432:5432 -e POSTGRES_PASSWORD=asdf -d postgres


asdf@asdf:~$ sudo docker run --name some-postgres -p 0.0.0.0:5432:5432 -e POSTGRES_PASSWORD=asdf -d postgres
50221ab7130781990a35eb65d2a04da42082e75cf3eef0644983a0ab5d9436a1
asdf@asdf:~$ docker ps
Got permission denied while trying to connect to the Docker daemon socket at unix:///var/run/docker.sock: Get http://%2Fvar%2Frun%2Fdocker.sock/v1.40/containers/json: dial unix /var/run/docker.sock: connect: permission denied
asdf@asdf:~$ sudo docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                    PORTS                    NAMES
50221ab71307        postgres            "docker-entrypoint.s…"   41 seconds ago      Up 40 seconds             0.0.0.0:5432->5432/tcp   some-postgres
98f8fef55791        superset_image      "/usr/bin/docker-ent…"   40 minutes ago      Up 40 minutes (healthy)   0.0.0.0:8080->8088/tcp   superset
asdf@asdf:~$ 

postgresql+psycopg2://username:password@localhost:5432/mydb
postgresql+psycopg2://postgres:asdf@localhost:5432/postgres



docker run -d -p 8080:8088 --name superset superset_image
docker run -d -p 8080:8088 --name superset superset_image_v2



docker exec -it superset superset fab create-admin \
               --username admin \
               --firstname Superset \
               --lastname Admin \
               --email admin@superset.com \
               --password admin
               
               

docker exec -it superset superset db upgrade


docker exec -it superset superset load_examples




# asdf

> *`docker pull postgres`*


> asdf@asdf:~$ *`sudo docker pull postgres`*

> root@asdf:~# *`docker run --name some-postgres -p 5432:5432 -e POSTGRES_PASSWORD=mysecretpassword -d postgres`*

# superset

> root@asdf:~# *`docker pull apache/superset`*

# Start a superset instance on port 8080


> root@asdf:~# *`docker run -d -p 8080:8088 --name superset apache/superset`*

# Initialize a local Superset Instance

## Setup your local admin account

> *`docker exec -it superset superset fab create-admin \
               --username admin \
               --firstname Superset \
               --lastname Admin \
               --email admin@superset.com \
               --password admin`*


root@asdf:~# docker exec -it superset superset fab create-admin \
>                --username admin \
>                --firstname Superset \
>                --lastname Admin \
>                --email admin@superset.com \
>                --password admin
logging was configured successfully
2021-08-29 15:54:20,745:INFO:superset.utils.logging_configurator:logging was configured successfully
2021-08-29 15:54:20,755:INFO:root:Configured event logger of type <class 'superset.utils.log.DBEventLogger'>
/usr/local/lib/python3.7/site-packages/flask_caching/__init__.py:202: UserWarning: Flask-Caching: CACHE_TYPE is set to null, caching is effectively disabled.
  "Flask-Caching: CACHE_TYPE is set to null, "
Recognized Database Authentications.
Admin User admin created.


## Migrate local DB to latest

> *`docker exec -it superset superset db upgrade`*


### Load Examples

> *`docker exec -it superset superset load_examples`*

### Setup roles
